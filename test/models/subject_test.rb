require 'test_helper'

class SubjectTest < ActiveSupport::TestCase
 
  def setup
    Subject.cache_delete_all
    Subject.delete_all
  end

  def teardown
    Subject.cache_delete_all
    Subject.delete_all
  end

  test "latest" do
    count = Subject.latest.count
    assert count == 0, 'latest:0'

    20.times{|i|FactoryGirl.create(:subject)}

    count = Subject.latest(0).count
    assert count == 0, 'latest:0'

    count = Subject.latest(1).count
    assert count == 1, 'latest:1'

    count = Subject.latest.count
    assert count == 10, 'latest:10'

    count = Subject.latest(10).count
    assert count == 10, 'latest:10'

    count = Subject.latest(20).count
    assert count == 20, 'latest:20'

    count = Subject.latest(30).count
    assert count == 20, 'latest:20'
  end
end
