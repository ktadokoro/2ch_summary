FactoryGirl.define do
  factory :subject do
    sequence(:id) {|n| n }
    blog_id 1
    sequence(:title) {|n| "title_#{n}" }
    sequence(:content) {|n| "content_#{n}" }
    site_url "http://www.google.co.jp"
    created_at '2013-05-01 03:28:00'
    updated_at '2013-05-01 03:28:00'
  end
end
