FactoryGirl.define do
  factory :blog do
    sequence(:id) {|n| n }
    sequence(:name) {|n| "name_#{n}" }
    site_url "http://www.google.co.jp"
    rss_url "http://www.google.co.jp/rss"
  end
end

