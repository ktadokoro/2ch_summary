FactoryGirl.define do
  factory :blog_ranking do
    sequence(:id) {|n| n }
    blog_id 1
    rank 0
    point 0
    target_date '2013-05-01'
  end
end

