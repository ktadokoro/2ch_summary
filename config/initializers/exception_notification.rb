TwochSummary::Application.config.middleware.use ExceptionNotification::Rack,
  email: {
    email_prefix:         "[#{Rails.env}][2chstream] ",
    sender_address:       "2chstream@gmail.com",
    exception_recipients: ["2chstream@gmail.com"]
}
