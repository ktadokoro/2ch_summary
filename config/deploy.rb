# capistranoの出力がカラーになる
require 'capistrano_colors'

# deploy 時に自動で bundle install
require 'bundler/capistrano'

# deploy 時に自動で rake assets:precompile
#load 'deploy/assets'

set :default_environment, {
  'RBENV_ROOT' => '$HOME/.rbenv',
  'PATH' => "$HOME/.rbenv/shims:$HOME/.rbenv/bin:$PATH"
}
set :bundle_flags, "--quiet --binstubs --shebang ruby-local-exec"

# SSHの設定
set :user, "webapp"
ssh_options[:port] = "22"
ssh_options[:forward_agent] = true
default_run_options[:pty] = true


# アプリケーションの設定
set :application, '2ch_summary'
set :domain, 'www13399ui.sakura.ne.jp'
set :rails_env, 'production'

set :repository, 'https://ktadokoro@bitbucket.org/ktadokoro/2ch_summary.git'
set :branch, 'master'

set :deploy_via, :copy
set :deploy_to, '/var/webapp/2ch_summary'
set :use_sudo, false

server domain, :app, :web

# 再起動後に古い releases を cleanup
after 'deploy:restart', 'deploy:cleanup'

# Unicorn 用の設定
set :unicorn_config, "#{current_path}/config/unicorn.rb"
set :unicorn_bin, 'unicorn'
set :unicorn_pid, '/var/run/unicorn/unicorn_2ch_summary.pid'

# Unicorn 用のデーモン操作タスク
def remote_file_exists?(full_path)
  capture("if [ -e #{full_path} ]; then echo 'true'; fi").strip == 'true'
end

def process_exists?(pid_file)
  capture("ps -p `cat #{pid_file}`; true").strip.split("\n").size == 2
end

namespace :deploy do
  desc 'Start Unicorn'
  task :start, roles: :app, except: {no_release: true} do
    if remote_file_exists? unicorn_pid
      if process_exists? unicorn_pid
        logger.important 'Unicorn is already running!', 'Unicorn'
        next
      end
    end

    logger.important 'Starting Unicorn...', 'Unicorn'
    run "cd #{current_path} && bundle exec unicorn -c #{unicorn_config} -E #{rails_env} -D"
  end

  desc 'Stop Unicorn'
  task :stop, :roles => :app, :except => {:no_release => true} do
    if remote_file_exists? unicorn_pid
      if process_exists? unicorn_pid
        logger.important 'Stopping Unicorn...', 'Unicorn'
        run "kill -s QUIT `cat #{unicorn_pid}`"
      else
        run "rm #{unicorn_pid}"
        logger.important 'Unicorn is not running.', 'Unicorn'
      end
    else
      logger.important 'No PIDs found. Check if unicorn is running.', 'Unicorn'
    end
  end

  desc 'Reload Unicorn'
  task :reload, :roles => :app, :except => {:no_release => true} do
    if remote_file_exists? unicorn_pid
      logger.important 'Reloading Unicorn...', 'Unicorn'
      run "kill -s HUP `cat #{unicorn_pid}`"
    else
      logger.important 'No PIDs found. Starting Unicorn...', 'Unicorn'
      run "cd #{current_path} && bundle exec unicorn -c #{unicorn_config} -E #{rails_env} -D"
    end
  end

  desc 'Restart Unicorn'
  task :restart, :roles => :app, :except => {:no_release => true} do
    stop
    start
  end
end
