TwochSummary::Application.routes.draw do
  root 'root#index'
  get "root/blog_site_redirect/:id" => "root#blog_site_redirect", as: :root_blog_site_redirect

  #resources :subjects
  get "/subjects/index/" => "subjects#index", as: :subjects_index
  get "/subjects/feed/" => "subjects#feed"

  #resources :rankings
  #get "/rankings/:yy/:mm/:dd" => "rankings#view"

  get "/subject_rankings/:kind/" => "subject_rankings#index"
  get "/subject_rankings/:kind/:yy/:mm/:dd" => "subject_rankings#index"
  #get "/subject_rankings/feed/" => "subject_rankings#feed"
  resources :subject_rankings

  get "/blog_rankings/:kind/" => "blog_rankings#index"
  get "/blog_rankings/:kind/:yy/:mm/:dd" => "blog_rankings#index"
  get "/blog_rankings/feed/" => "blog_rankings#feed"
  resources :blog_rankings

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
