class SubjectsController < ApplicationController
  #before_action :set_book, only: [:show, :edit, :update, :destroy]
  #before_action only: [:show, ]

  def index
    @today = Date.today
    @yesterday = Date.today.prev_day
    @search_form = SearchForm.new params[:search_form]
    @subjects = Subject.order("created_at DESC").page(params[:page]).per(30)
    if @search_form.q.present?
      @subjects = @subjects.where("title like '%#{@search_form.q}%'")
    end

    @search_from_path = 'subjects/search_from'
  end

  def feed
    @subjects = Subject.all[0..100]
    respond_to do |format|
      format.rss
    end
  end
end
