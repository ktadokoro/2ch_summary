module RankingsController
  def ranking_class
  end

  def index
    if not params[:yy]
      date = Date.today
    else
      yy = params[:yy].to_i
      mm = params[:mm].to_i
      dd =  params[:dd].to_i
      date = Date.new(yy,mm,dd)
    end
   cls = ranking_class
   rankings = cls.get_ranking(params[:kind].to_s, date)
   @rankings = Kaminari.paginate_array(rankings).page(params[:page]).per(30)
  end
end
