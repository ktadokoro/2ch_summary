class RootController < ApplicationController
    def index
        @subjects = Subject.latest(10)
        @today = Date.today
        @blog_rankings = BlogRanking.day_ranking(@today)
        @subject_rankings = SubjectRanking.day_ranking(@today)
        @yesterday = Date.today.prev_day
        @yesterday_blog_rankings = BlogRanking.day_ranking(@yesterday)
        @yesterday_subject_rankings = SubjectRanking.day_ranking(@yesterday)

        # 昨日から1週間、1ヶ月分
        # 本日分は変化が激しいので、固定しておきたい
        @week_blog_rankings = BlogRanking.week_ranking
        @week_subject_rankings = SubjectRanking.week_ranking
        @month_blog_rankings = BlogRanking.month_ranking
        @month_subject_rankings = SubjectRanking.month_ranking

        @search_from_path = 'root/search_from'
        @search_form = SearchForm.new params[:search_form]
        if @search_form.q.present?
          @subjects = @subjects.where("title like '%#{@search_form.q}%'")
        end
    end

    def blog_site_redirect
        # subjectのsite_urlにリダイレクトさせてランキングのポイントを更新させる
        subject = Subject.find(:first, :conditions => ["id = ?", params[:id]])

        if subject.blank?
            redirect_to "/"
        else
            BlogPoint.update_point(subject)
            SubjectPoint.update_point(subject)

            redirect_to "#{subject.site_url}"
        end
    end
end
