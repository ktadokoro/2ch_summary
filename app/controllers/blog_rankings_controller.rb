class BlogRankingsController < ApplicationController
  include RankingsController

  def ranking_class
    BlogRanking
  end

  def index
    @today = Date.today
    @yesterday = Date.today.prev_day
    @rankings = super

    @search_form = SearchForm.new params[:search_form]
    if @search_form.q.present?
      regexp = /#{@search_form.q}/
      rankings = @rankings.select{|ranking|regexp =~ ranking.blog_point.blog.name}
      @rankings = Kaminari.paginate_array(rankings).page(params[:page]).per(30)
    end

    @search_from_path = 'blog_rankings/search_from'
    case params[:kind]
    when 'day','yesterday'
      yy = params[:yy].to_i
      mm = params[:mm].to_i
      dd =  params[:dd].to_i
      date = Date.new(yy,mm,dd)
    when 'week','month'
      date = Date.today
    else
      date = Date.today
    end
    @search_from_params = {:kind => params[:kind], :day => date}

    respond_to do |format|
      format.html
      format.rss
    end
  end
end
