module SubjectRankingsHelper
  def subject_ranking_link_to(str, kind, options={})
    target_date = Date.today

    link = ''
    case kind
    when 'today'
      link = "/subject_rankings/day/#{target_date.year}/#{target_date.month}/#{target_date.day}"
    when 'yesterday'
      target_date = target_date.prev_day
      link = "/subject_rankings/day/#{target_date.year}/#{target_date.month}/#{target_date.day}"
    when 'week'
      link = "/subject_rankings/week/"
    when 'month'
      link = "/subject_rankings/month/"
    end

    if options[:class]
      cls = "class = \"#{options[:class]}\""
    end
    if options[:id]
      id = "id = \"#{options[:class]}\""
    end

    return "<a #{cls} #{id} href=\"#{link}\">#{str}</a>".html_safe
  end
end
