module ApplicationHelper
  def logo_tag
    path = Dir.glob("public/images/logo/*").shuffle[0]
    basename = File.basename(path)
    path = File.join('images', 'logo', basename)
    return "<img alt=\"logo\" src=\"/#{path}\" />".html_safe
  end

  def hatena_users(url)
    "<span><a href=\"http://b.hatena.ne.jp/entry/#{url}\"><img src=\"http://b.hatena.ne.jp/entry/image/#{url}\"></a></span>".html_safe
  end
end
