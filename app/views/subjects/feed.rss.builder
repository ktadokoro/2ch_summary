xml.instruct! :xml, :version => "1.0" 
xml.rss :version => "2.0" do
  xml.channel do
    xml.title "2chまとめストリーム"
    xml.description "2chまとめサイトの新着情報"
    xml.link "http://www.2chstream.com#{subjects_index_path}"

    for subject in @subjects
      xml.item do
        xml.title subject.title
        xml.description subject.blog.name
        xml.pubDate subject.created_at.to_s(:rfc822)
        xml.link "http://www.2chstream.com#{root_blog_site_redirect_path(subject)}"
        xml.guid subjects_index_path
      end
    end
  end
end
