xml.instruct! :xml, :version => "1.0" 
xml.rss :version => "2.0" do
  xml.channel do
    xml.title "2chまとめストリーム"
    xml.description "2chまとめサイトの新着情報"
    xml.link "http://www.2chstream.com#{blog_rankings_path}"

    for ranking in @rankings
      xml.item do
        xml.title ranking.blog_point.blog.name
        xml.description ranking.blog_point.blog.name
        xml.pubDate ranking.created_at.to_s(:rfc822)
        xml.link "http://www.2chstream.com#{root_blog_site_redirect_path(ranking)}"
        xml.guid "http://www.2chstream.com#{blog_rankings_path}"
      end
    end
  end
end
