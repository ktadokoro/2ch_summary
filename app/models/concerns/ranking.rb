module Ranking
  #extend module

  #overwrite, please!
  def ranking_class
  end

  #overwrite, please!
  def cache_key_ranking_day
  end

  #overwrite, please!
  def cache_key_ranking_week
  end

  #overwrite, please!
  def cache_key_ranking_month
  end

  def get_ranking(kind="day", target_date)
    case kind
    when 'day'
      day_ranking(target_date)
    when 'week'
      week_ranking
    when 'month'
      month_ranking
    else
      day_ranking(target_date)
    end
  end

  def day_ranking(target_date)
    # target_dateのランキングを取得する
    cls = ranking_class

    cache_key = cache_key_ranking_day % [target_date.strftime("%Y-%m-%d")]
    rankings = Rails.cache.read cache_key
    if not rankings or rankings.empty?
      rankings = cls.where(:kind => cls::KIND_DAY, :target_date => target_date).order("rank")
      Rails.cache.write cache_key, rankings.to_a, expires_in: 15.minutes
    end

    return rankings
  end

  def week_ranking
    # ランキングを取得する
    cls = self.ranking_class

    cache_key = cache_key_ranking_week
    rankings = Rails.cache.read cache_key
    if not rankings or rankings.empty?
      rankings = cls.where(:kind => cls::KIND_WEEK).order("rank")
      Rails.cache.write cache_key, rankings.to_a, expires_in: 60.minutes
    end

    return rankings
  end

  def month_ranking
    # ランキングを取得する
    cls = self.ranking_class

    cache_key = cache_key_ranking_month
    rankings = Rails.cache.read cache_key
    if not rankings or rankings.empty?
      rankings = cls.where(:kind => cls::KIND_MONTH).order("rank")
      Rails.cache.write cache_key, rankings.to_a, expires_in: 60.minutes
    end

    return rankings
  end

  def self.cache_delete_all(target_date)
    cache_key = cache_key_ranking_day % [target_date.strftime("%Y-%m-%d")]
    Rails.cache.write cache_key, nil, expires_in: 15.minutes

    cache_key = cache_key_ranking_week
    Rails.cache.write cache_key, nil, expires_in: 15.minutes

    cache_key = cache_key_ranking_month
    Rails.cache.write cache_key, nil, expires_in: 15.minutes
  end
end
