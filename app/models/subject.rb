class Subject < ActiveRecord::Base
    require 'cache/blog_cache'
    include Blog_cache

#    belongs_to :blog
    CACHE_KEY_LATEST = 'subject:latest'
    scope :subject, -> (q) {where 'subject like ?', "%#{q}"}

    def self.cache_delete_all
        cache_key = CACHE_KEY_LATEST
        Rails.cache.write cache_key, nil, expires_in: 15.minutes
    end

    def self.latest(limit = 10)
        # 最新のデータをlimit分取得する
        cache_key = CACHE_KEY_LATEST
        subject = Rails.cache.read cache_key
        if not subject or subject.count < limit
            subject = Subject.order("created_at DESC").limit(limit)
            Rails.cache.write cache_key, subject.to_a, expires_in: 15.minutes
        end
        return subject
    end 
end
