module BlogPoint_cache
    CACHE_KEY_BLOG_POINT = 'blog_point:%s'

    def blog_point
        cache_key = CACHE_KEY_BLOG_POINT % [self.blog_point_id]
        blog_point = Rails.cache.read cache_key
        if not blog_point
            blog_point = BlogPoint.find(self.blog_point_id)
            Rails.cache.write cache_key, blog_point, expires_in: 15.minutes
        end
        return blog_point
    end
end
