module Subject_cache
    CACHE_KEY_SUBJECT = 'subject:%s'

    def subject
        cache_key = CACHE_KEY_SUBJECT % [self.subject_id]
        subject = Rails.cache.read cache_key
        if not subject
            subject = Subject.find(self.subject_id)
            Rails.cache.write cache_key, subject, expires_in: 1.hour
        end
        return subject
    end
end
