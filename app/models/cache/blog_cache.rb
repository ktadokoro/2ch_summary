module Blog_cache
    CACHE_KEY_BLOG = 'blog:%s'

    def blog
        cache_key = CACHE_KEY_BLOG % [self.blog_id]
        blog = Rails.cache.read cache_key
        if not blog
            blog = Blog.find(self.blog_id)
            Rails.cache.write cache_key, blog, expires_in: 1.hour
        end
        return blog
    end
end
