module SubjectPoint_cache
    CACHE_KEY_SUBJECT_POINT = 'subject_point:%s'

    def subject_point
        cache_key = CACHE_KEY_SUBJECT_POINT % [self.subject_point_id]
        subject_point = Rails.cache.read cache_key
        if not subject_point
            subject_point = SubjectPoint.find(self.subject_point_id)
            Rails.cache.write cache_key, subject_point, expires_in: 15.minutes
        end
        return subject_point
    end
end
