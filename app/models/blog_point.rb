class BlogPoint < ActiveRecord::Base
#  belongs_to :blog
  require 'cache/blog_cache'
  include Blog_cache
  attr_accessor :rank

  def self.update_point(subject)
    target_date = Date.today().strftime("%Y-%m-%d")
    blog_point = BlogPoint.where("blog_id = ? and target_date = ?", subject.blog.id, target_date).first

    if blog_point.blank?
      blog_point = BlogPoint.new
      blog_point.blog_id = subject.blog.id
      blog_point.point = 1
      blog_point.target_date = target_date
      blog_point.save()
    else
      blog_point.point += 1
      blog_point.save()
    end

    return blog_point
  end
end
