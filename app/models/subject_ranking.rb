class SubjectRanking < ActiveRecord::Base
  belongs_to :subject
  belongs_to :subject_point
  require 'cache/subject_cache'
  include Subject_cache
  require 'cache/subject_point_cache'
  include SubjectPoint_cache
  extend Ranking

  KIND_DAY = 0
  KIND_WEEK = 1
  KIND_MONTH = 2

  def self.ranking_class
    SubjectRanking
  end

  def self.cache_key_ranking_day
    'subject_ranking:day_ranking:%s'
  end

  def self.cache_key_ranking_week
    #'subject_ranking:week_ranking:%s'
    'subject_ranking:week_ranking'
  end

  #overwrite, please!
  def self.cache_key_ranking_month
    #'subject_ranking:month_ranking:%s'
    'subject_ranking:month_ranking'
  end
end
