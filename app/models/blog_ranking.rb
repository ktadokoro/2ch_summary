class BlogRanking < ActiveRecord::Base
  belongs_to :blog
  belongs_to :blog_point
  require 'cache/blog_cache'
  include Blog_cache
  require 'cache/blog_point_cache'
  include BlogPoint_cache
  extend Ranking

  KIND_DAY = 0
  KIND_WEEK = 1
  KIND_MONTH = 2

  def self.ranking_class
    BlogRanking
  end

  def self.cache_key_ranking_day
    'blog_ranking:day_ranking:%s'
  end

  def self.cache_key_ranking_week
    'blog_ranking:week_ranking'
  end

  #overwrite, please!
  def self.cache_key_ranking_month
    'blog_ranking:month_ranking'
  end
end
