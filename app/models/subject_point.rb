class SubjectPoint < ActiveRecord::Base
  belongs_to :subject
  attr_accessor :rank
  require 'cache/subject_cache'
  include Subject_cache

  def self.update_point(subject)
    target_date = Date.today().strftime("%Y-%m-%d")
    subject_point = SubjectPoint.where("subject_id = ? and target_date = ?", subject.id, target_date).first

    if subject_point.blank?
      subject_point = SubjectPoint.new
      subject_point.subject_id = subject.id
      subject_point.point = 1
      subject_point.target_date = target_date
      subject_point.save()
    else
      subject_point.point += 1
      subject_point.save()
    end

    return subject_point
  end
end
