require 'parallel'
require 'feed-normalizer'
require 'time'
require 'date'

def parse_date(date)
    begin
        time = Time.parse(date)
        return DateTime.parse(time.to_s)
    rescue => exc
        return
    end
end

def parse_rss_url(blog)
    begin
        html = open(blog.rss_url)
        return FeedNormalizer::FeedNormalizer.parse(html)
    rescue => exc
        Rails.logger.info('url_error: exc:%s blog_id:%s rss_url:%s' % [exc, blog.id, blog.rss_url])
        return
    end
end

def update_subject(blog, rss)
    rss.entries.map do |item|
        if item.title.to_s
            date = parse_date(item.last_updated.to_s)
            next_date = DateTime.now + 1
            if date and blog.updated_at < date and date < next_date
                begin
                    Subject.create(:title => item.title.to_s, :blog_id => blog.id, :content => item.description.to_s, :site_url => item.url.to_s, :created_at => date.to_s(:db), :updated_at => date.to_s(:db)) 
                rescue => exc
                    Rails.logger.info('inset_subject_error: blog_id:%s title:%s date:%s' % [blog.id, item.title, date])
                end
            else
                Rails.logger.debug('not update subject: blog_id:%s title:%s date:%s' % [blog.id, item.title, date])
            end
        end
    end
end

def update_blog(blog, now)
    blog.updated_at = now
    blog.save()
end

def main
    rss_list = []
    now = Time.now.to_s(:db)
    blogs = Blog.where(:is_enable => true)

    Parallel.each(blogs, in_threads: blogs.count()) {|blog|
        rss = parse_rss_url(blog)
        if rss
            rss_list << {:rss => rss, :blog_id => blog.id}
        end
    }

    rss_list.each do |rss|
        blog = Blog.find(rss[:blog_id])
        update_subject(blog, rss[:rss])
        update_blog(blog, now)
    end
end

task :update_subject => :environment do
  main()
end
