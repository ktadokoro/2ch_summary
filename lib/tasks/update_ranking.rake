class UpdateBlogRanking
  def self.day(target_date)
    rankings = BlogPoint.where(:target_date => target_date).order("point DESC").limit(100)
    rankings.each_with_index do |ranking, i|
      blog_ranking = BlogRanking.where(kind: BlogRanking::KIND_DAY, rank: i+1, target_date: target_date).first
      if blog_ranking
        blog_ranking.blog_point = ranking
        blog_ranking.save
      else
        BlogRanking.create(blog_point: ranking, kind: BlogRanking::KIND_DAY, rank: i+1, target_date: target_date)
      end
    end
  end

  def self.week(target_date)
    before_date = target_date.prev_day(7)
    rankings = BlogPoint.where(target_date: before_date..target_date).group("blog_id").order('point DESC').limit(100)
    rankings.each_with_index do |ranking, i|
      blog_ranking = BlogRanking.where(kind: BlogRanking::KIND_WEEK, rank: i+1).first
      if blog_ranking
        blog_ranking.blog_point = ranking
        blog_ranking.save
      else
        BlogRanking.create(blog_point: ranking, kind: BlogRanking::KIND_WEEK, rank: i+1)
      end
    end
  end

  def self.month(target_date)
    before_date = target_date.prev_day(30)
    rankings = BlogPoint.where(target_date: before_date..target_date).group("blog_id").order('point DESC').limit(100)
    rankings.each_with_index do |ranking, i|
      blog_ranking = BlogRanking.where(kind: BlogRanking::KIND_MONTH, rank: i+1).first
      if blog_ranking
        blog_ranking.blog_point = ranking
        blog_ranking.save
      else
        BlogRanking.create(blog_point: ranking, kind: BlogRanking::KIND_MONTH, rank: i+1)
      end
    end
  end
end

class UpdateSubjectRanking
  def self.day(target_date)
    rankings = SubjectPoint.where(:target_date => target_date).order("point DESC").limit(100)
    rankings.each_with_index do |ranking, i|
      subject_ranking = SubjectRanking.where(kind: SubjectRanking::KIND_DAY, rank: i+1, target_date: target_date).first
      if subject_ranking
        subject_ranking.subject_point = ranking
        subject_ranking.save
      else
        SubjectRanking.create(subject_point: ranking, kind: SubjectRanking::KIND_DAY, rank: i+1, target_date: target_date)
      end
    end
  end

  def self.week(target_date)
    before_date = target_date.prev_day(7)
    rankings = SubjectPoint.where(target_date: before_date..target_date).group("subject_id").order('point DESC').limit(100)
    rankings.each_with_index do |ranking, i|
      subject_ranking = SubjectRanking.where(kind: SubjectRanking::KIND_WEEK, rank: i+1).first
      if subject_ranking
        subject_ranking.subject_point = ranking
        subject_ranking.save
      else
        SubjectRanking.create(subject_point: ranking, kind: SubjectRanking::KIND_WEEK, rank: i+1)
      end
    end
  end

  def self.month(target_date)
    before_date = target_date.prev_day(30)
    rankings = SubjectPoint.where(target_date: before_date..target_date).group("subject_id").order('point DESC').limit(100)
    rankings.each_with_index do |ranking, i|
      subject_ranking = SubjectRanking.where(kind: SubjectRanking::KIND_MONTH, rank: i+1).first
      if subject_ranking
        subject_ranking.subject_point = ranking
        subject_ranking.save
      else
        SubjectRanking.create(subject_point: ranking, kind: SubjectRanking::KIND_MONTH, rank: i+1)
      end
    end
  end
end

def get_date(target_date=nil)
  if target_date
    target_date = Date.strptime(target_date, "%Y-%m-%d")
  else
    target_date = Date.today
  end
end

namespace :ranking do
  namespace :blog do
    desc 'day'
    task :day,'day'
    task :day => :environment do |t, args|
      tartget_date = get_date(args.day)
      UpdateBlogRanking.day tartget_date
    end

    desc 'week'
    task :week,'day'
    task :week => :environment do |t, args|
      tartget_date = get_date(args.day)
      UpdateBlogRanking.week tartget_date
    end

    desc 'month'
    task :month,'day'
    task :month => :environment do |t, args|
      tartget_date = get_date(args.day)
      UpdateBlogRanking.month tartget_date
    end
  end

  namespace :subject do
    desc 'day'
    task :day, 'day'
    task :day => :environment do |t, args|
      tartget_date = get_date(args.day)
      UpdateSubjectRanking.day tartget_date
    end

    desc 'week'
    task :week, 'day'
    task :week => :environment do |t, args|
      tartget_date = get_date(args.day)
      UpdateSubjectRanking.week tartget_date
    end

    desc 'month'
    task :month, 'day'
    task :month => :environment do |t, args|
      tartget_date = get_date(args.day)
      UpdateSubjectRanking.month tartget_date
    end
  end
end
