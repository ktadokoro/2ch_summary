# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20130520065839) do

  create_table "blog_points", force: true do |t|
    t.integer  "blog_id"
    t.integer  "point"
    t.date     "target_date"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "blog_points", ["blog_id"], name: "index_blog_points_on_blog_id", using: :btree
  add_index "blog_points", ["target_date"], name: "index_blog_points_on_target_date", using: :btree

  create_table "blog_rankings", force: true do |t|
    t.integer  "blog_point_id"
    t.integer  "kind"
    t.integer  "rank"
    t.date     "target_date"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "blog_rankings", ["blog_point_id"], name: "index_blog_rankings_on_blog_point_id", using: :btree
  add_index "blog_rankings", ["kind", "rank", "target_date"], name: "index_blog_rankings_on_kind_and_rank_and_target_date", unique: true, using: :btree

  create_table "blogs", force: true do |t|
    t.string   "name",                      null: false
    t.string   "site_url",                  null: false
    t.string   "rss_url",                   null: false
    t.boolean  "is_enable",  default: true
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "subject_points", force: true do |t|
    t.integer  "subject_id"
    t.integer  "point"
    t.date     "target_date"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "subject_points", ["subject_id"], name: "index_subject_points_on_subject_id", using: :btree
  add_index "subject_points", ["target_date"], name: "index_subject_points_on_target_date", using: :btree

  create_table "subject_rankings", force: true do |t|
    t.integer  "subject_point_id"
    t.integer  "kind"
    t.integer  "rank"
    t.date     "target_date"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "subject_rankings", ["kind", "rank", "target_date"], name: "index_subject_rankings_on_kind_and_rank_and_target_date", unique: true, using: :btree
  add_index "subject_rankings", ["subject_point_id"], name: "index_subject_rankings_on_subject_point_id", using: :btree

  create_table "subjects", force: true do |t|
    t.integer  "blog_id"
    t.string   "title",      null: false
    t.text     "content",    null: false
    t.string   "site_url",   null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "subjects", ["blog_id", "title", "created_at"], name: "index_subjects_on_blog_id_and_title_and_created_at", using: :btree

end
