class CreateBlogs < ActiveRecord::Migration
  def change
    create_table :blogs do |t|
      t.string :name, :null => false # ブログ名
      t.string :site_url, :null => false # サイトurl
      t.string :rss_url, :null => false # rssのurl
      t.boolean :is_enable, :default => true # 有効か

      t.timestamps
    end
  end
end
