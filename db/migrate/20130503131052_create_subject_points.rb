class CreateSubjectPoints < ActiveRecord::Migration
  def change
    create_table :subject_points do |t|
      t.references :subject, index: true
      t.integer :point
      t.date :target_date

      t.timestamps
    end
    add_index :subject_points, [:target_date]
  end
end
