class CreateBlogRankings < ActiveRecord::Migration
  def change
    create_table :blog_rankings do |t|
      t.references :blog_point, index: true
      t.integer :kind #day,week,month
      t.integer :rank #1位,2位,....
      t.date :target_date #weekの場合は、この日から1週間以内のランク、monthも同様

      t.timestamps
    end
    add_index :blog_rankings, [:kind, :rank, :target_date], :unique => true
  end
end
