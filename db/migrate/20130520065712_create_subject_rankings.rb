class CreateSubjectRankings < ActiveRecord::Migration
  def change
    create_table :subject_rankings do |t|
      t.references :subject_point, index: true
      t.integer :kind #day,week,month
      t.integer :rank #1位,2位,....
      t.date :target_date

      t.timestamps
    end
    add_index :subject_rankings, [:kind, :rank, :target_date], :unique => true
  end
end
