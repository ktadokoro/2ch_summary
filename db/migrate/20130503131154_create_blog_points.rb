class CreateBlogPoints < ActiveRecord::Migration
  def change
    create_table :blog_points do |t|
      t.references :blog, index: true
      t.integer :point
      t.date :target_date

      t.timestamps
    end
    add_index :blog_points, [:target_date]
  end
end
