class CreateSubjects < ActiveRecord::Migration
  def change
    create_table :subjects do |t|
      t.references("blog")
      t.string :title, :null => false # タイトル
      t.text :content, :null => false # 内容
      t.string :site_url, :null => false # サイトurl

      t.timestamps
    end
    add_index :subjects, [:blog_id, :title, :created_at]
  end
end
