# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
#
require 'csv'

#f = CSV.open("db/blog.csv", "r")
#f.each do |row|
#    Blog.create(:name => row[0], :site_url => row[1], :rss_url => row[2])
#end

#100000.times do |i|
#  blog_id = Blog.all.shuffle[0].id
#  title = (("a".."z").to_a + ("A".."Z").to_a + (0..9).to_a).shuffle[0..7].join
#  content = (("a".."z").to_a + ("A".."Z").to_a + (0..9).to_a).shuffle[0..7].join
#  site_url='http://google.co.jp'
#  subjects = Subject.create(blog_id: blog_id, title: title, content: content, site_url: site_url)
#end

##target_date = '2013-05-03'
#today = Date.today
#7.times do |i|
#  target_date = today.prev_day(i-1)
#
#  #subjects = Subject.all.shuffle
#  subjects = Subject.all
#  i=1
#  point=100000
#  subjects.each do |subject|
#    SubjectPoint.create(subject_id: subject.id, point: point, target_date: target_date)
#    i+=1
#    point-=1
#  end
#
#  #blogs = Blog.all.shuffle
#  blogs = Blog.all
#  i=1
#  point=100000
#  blogs.each do |blog|
#    BlogPoint.create(blog_id: blog.id, point: point, target_date: target_date)
#    i+=1
#    point-=1
#  end
#end

today = Date.today.prev_day(1)
target_date = today
rankings = SubjectPoint.where(:target_date => target_date).order("point DESC").limit(100)
rankings.each_with_index do |ranking, i|
  SubjectRanking.create(rank: i+1, subject_point: ranking, kind: 0, target_date: target_date)
end

before_date = today.prev_day(7)
rankings = SubjectPoint.where(target_date: before_date..target_date).group("subject_id").order('point DESC').limit(100)
rankings.each_with_index do |ranking, i|
  SubjectRanking.create(rank: i+1, subject_point: ranking, kind: 1, target_date: target_date)
end

before_date = today.prev_day(30)
rankings = SubjectPoint.where(target_date: before_date..target_date).group("subject_id").order('point DESC').limit(100)
rankings.each_with_index do |ranking, i|
  SubjectRanking.create(rank: i+1, subject_point: ranking, kind: 2, target_date: target_date)
end


target_date = today
rankings = BlogPoint.where(:target_date => target_date).order("point DESC").limit(100)
rankings.each_with_index do |ranking, i|
  BlogRanking.create(rank: i+1, blog_point: ranking, kind: 0, target_date: target_date)
end

before_date = today.prev_day(7)
rankings = BlogPoint.where(target_date: before_date..target_date).group("blog_id").order('point DESC').limit(100)
rankings.each_with_index do |ranking, i|
  BlogRanking.create(rank: i+1, blog_point: ranking, kind: 1, target_date: target_date)
end

before_date = today.prev_day(30)
rankings = BlogPoint.where(target_date: before_date..target_date).group("blog_id").order('point DESC').limit(100)
rankings.each_with_index do |ranking, i|
  BlogRanking.create(rank: i+1, blog_point: ranking, kind: 2, target_date: target_date)
end
